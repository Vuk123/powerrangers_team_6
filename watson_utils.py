from watson_developer_cloud import ConversationV1
import json

class Watson:
    def __init__(self, username, password, version, workspace_id):
        self.username = username
        self.password = password
        self.version = version
        self.workspace_id = workspace_id
        self.conversation = ConversationV1(
            username=username,
            password=password,
            version=version
        )

    def message(self, msg):
        return self.conversation.message(workspace_id=self.workspace_id, message_input={'text': msg})


# waki = Watson('c557270d-2fe2-4af8-acbb-761b6c50a9f8', 'ixRwdUE8i6te', '2017-05-26',
#                '125870c1-9dc7-483d-9aa1-1afd4952a795')
# output = waki.message("Treba mi neki dobar kredit")
# print json.dumps(output, indent=4)
# # print output['output']
# print output['intents'][0]['intent']