from twilio.rest import Client
import pandas as pd
import random
import csv


class SMSHandler:
    # def __init__(self):
    account_sid = "AC072172a97c38abb7ae7cca48511ad8e3"
    auth_token = "e528aa4461b76e51e133dae07be41cc0"
    client = Client(account_sid, auth_token)

    @staticmethod
    def send_sms(recipient, msg):
        message = SMSHandler.client.api.account.messages.create(to=recipient,
                                                                from_="+14806307615",
                                                                body=msg)

    @staticmethod
    def send_to_user(user_id):
        df = pd.read_csv('podaci.csv', sep=',')
        phone = df[df['id'] == user_id]['phone'].values[0]
        if not str(phone).startswith('+'):
            phone = '+' + str(phone)
        code_int = str(random.randint(1111, 9999))
        code = str(code_int)
        df.set_value(df['id'] == user_id, 'kod', code)
        df.to_csv('podaci.csv', index=False)
        SMSHandler.send_sms(phone, "Unesite ove 4 cifre {}".format(str(code)))


    @staticmethod
    def check(user_id, code):
        df = pd.read_csv('podaci.csv', sep=',')
        kod = df[df['id'] == user_id]['kod'].values[0]
        df.set_value(df['id'] == user_id, 'kod', None)
        df.to_csv('podaci.csv', index=False)

        if code != kod:
            return 0
        else:
            return 1


#SMSHandler.send_to_user(1355375471236426)
#print(SMSHandler.check(1355375471236426, 1451))

# SMSHandler.send_sms("+381652258273", 'Da li ovo radi o da li?')
