import base64
import requests
import pandas as pd
import urllib


class MegaZord:
    def __init__(self, api_key):
        self.url = 'https://vision.googleapis.com/v1/images:annotate?key={}'.format(api_key)
        self.header = {'Content-Type': 'application/json'}

    def req(self, body):
        response = requests.post(self.url, headers=self.header, json=body)
        return response.json()


    @staticmethod
    def encode_image(image):
        image_content = image.read()
        return base64.b64encode(image_content).decode()

    @staticmethod
    def get_image_by_url(img_url, img_name):
        try:
            urllib.urlretrieve(img_url, "{}.jpg".format(img_name))
        except:
            raise ValueError


class Aladin:
    def __init__(self, description):
        self.description = description

    def score_ok(self):
        if self.description[0]['Score'] > 1.0:
            return True
        else:
            return False


class KreditEvaluator:
    def __init__(self, product=None, user_id=None):
        self.user = user_id
        self.product = product
        phones = ['mobile phone', 'phone', 'mobile', "smartphone", 'smart phone']
        laptops = ['laptop', 'computer', 'lap top']
        macbook = ['macbook', 'mac book pro', 'macbook air', 'mac book air', 'mac book']
        cameras = ['camera', 'photo camera']
        if self.product:
            if self.product.lower() in phones:
                self.product = 'mobilni'
            if self.product.lower() in cameras:
                self.product = 'fotoaparat'
            if self.product.lower() in laptops:
                self.product = 'laptop'
            if self.product.lower() in macbook:
                self.product = 'macbook'

    def get_max_money(self):
        # get from txt file info about user and return max credit value
        df = pd.read_csv('podaci.csv', sep=',')
        return df[df['id'] == int(self.user)]['max'].values[0]

    def is_ok_credit(self):
        # returns true if user is ok for credit
        df = pd.read_csv('podaci.csv', sep=',')
        return df[df['id'] == self.user]['status'].values[0]

    def get_product_price(self):
        df = pd.read_csv('products.csv', sep=',')
        return int(df[df['type'] == self.product]['price'].values[0])

