from urllib import urlopen
from bs4 import BeautifulSoup
import json
import pandas as pd
import re
import csv

html_template = "http://www.winwin.rs/laptop-i-tablet-racunari/laptop-notebook-racunari.html?p={}"
racunari = []
for x in xrange(2):

    html = urlopen(html_template.format(x))
    soup = BeautifulSoup(html, 'html.parser')

    for item in soup.find_all('li', attrs={'itemprop': 'itemListElement'}):
        temp = {"Cena": "", "Link": ""}
        #print(item)

        if (item.find('span', attrs={'class': 'tipped mp-price'})):
            cena = item.find('span', attrs={'class': 'tipped mp-price'}).text

        elif (item.find('span', attrs={'class': 'tipped strikethrough-price'})):
            cena = item.find('span', attrs={'class': 'tipped strikethrough-price'}).text
        else:
            cena ='0 din.'

        link = item.find('a', href=True)
        url = link['href']
        cena = cena.strip()[:-5]
        cena = cena.replace('.', '')
        cena = int(cena)

        temp['Cena'] = cena
        temp['Link'] = url
        racunari.append(temp)

keys = racunari[0].keys()

with open('result.json', 'w') as fp:
    json.dump(racunari, fp, indent=4)

with open('laptopovi.csv', 'a') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(racunari)
