from fysom import Fysom
from watson_utils import Watson

waki = Watson('75f9159c-e805-474a-9e11-c1fc0daaac75', 'pPB2jizKE078', '2017-05-26',
              'f292ef1a-a47e-4c5d-9b51-b4af28213a7f')


class Tree:

    def __init__(self):
        self.fsm = Fysom({'initial': 'hello',
                         'events': [
                             {'name': 'hello_ok', 'src': 'hello', 'dst': 'wait_for_image'},
                             {'name': 'no_image', 'src': 'wait_for_image', 'dst': 'invalid_image'},
                             {'name': 'image_sent', 'src': 'wait_for_image', 'dst': 'identify'},
                             {'name': 'image_ok', 'src': 'identify', 'dst': 'credit'},
                             {'name': 'image_not_ok', 'src': 'identify', 'dst': 'resend_image'},
                             {'name': 'image_sent', 'src': 'resend_image', 'dst': 'identify'},
                             {'name': 'good', 'src': 'identify', 'dst': 'done'}],
                         })


