import os

from utilis import MegaZord


class GoogleWrapper:
    api_key = 'AIzaSyAdLY2rSayaxBeAGxIcc2IyT7cWJLKGi1I'
    service = MegaZord(api_key)

    @classmethod
    def feature_finder(cls, image, url_flag=1, detection_type='LABEL', max_results=1):
        """
        Function for calling Google Vision API
        :param image: File or Image ULR depending on url flag variable, url is default
        :param url_flag: Flag if Image variable is file or URL,
        :param detection_type: LABEL, WEB, TEXT, LOGO
        :param max_results: Number of results in response
        :return: response from google
        """
        if url_flag:
            try:
                MegaZord.get_image_by_url(image, 'temp')
            except ValueError:
                print("Can not download that picture!!! Returning null...")
                return
            image = 'temp.jpg'
        with open(image, 'rb') as image:
            base64_image = MegaZord.encode_image(image)
            body = {
                'requests': [{
                    'image': {
                        'content': base64_image,
                    },
                    'features': [{
                        'type': '{}_DETECTION'.format(detection_type),
                        'maxResults': max_results,
                    }]

                }]
            }

        response = cls.service.req(body=body)

        results = []
        if detection_type == 'WEB':
            results = GoogleWrapper.parse_web_response(response)
        if detection_type == 'LABEL':
            results = GoogleWrapper.parse_label_response(response)
        if detection_type == 'TEXT':
            results = GoogleWrapper.parse_text_response(response)

        return results

    @classmethod
    def parse_web_response(cls, resp):
        det_type = 'web'
        result = []
        for x in xrange(len(resp['responses'][0]['{}Detection'.format(det_type)]['{}Entities'.format(det_type)])):
            entitet = resp['responses'][0]['{}Detection'.format(det_type)]['{}Entities'.format(det_type)][x][
                                   'description']
            score = resp['responses'][0]['{}Detection'.format(det_type)]['{}Entities'.format(det_type)][x]['score']

            if entitet not in ['Product design','Feature phone', 'Netbook']:
                result.append({"Entitet":entitet, "Score":score})
        return result

    @classmethod
    def parse_label_response(cls, resp):
        det_type = 'web'
        result = []
        for x in xrange(len(resp['responses'][0]['labelAnnotations'])):
            result.append({"Entitet": resp['responses'][0]['labelAnnotations'][x]['description'],
                           "Score": resp['responses'][0]['labelAnnotations'][x]['score']})
        return result

    @classmethod
    def parse_text_response(cls, resp):
        det_type = 'web'
        result = []
        for x in xrange(len(resp['responses'][0]['textAnnotations'])):
            result.append({"Text": resp['responses'][0]['textAnnotations'][x]['description']})
        return result




