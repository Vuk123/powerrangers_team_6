import os
import sys
import json
import requests
from flask import Flask, request
import redis
from watson_utils import Watson
from utilis import Aladin, KreditEvaluator
from sms import SMSHandler
from google_wrapper import GoogleWrapper

r = redis.StrictRedis(host='ec2-34-206-77-235.compute-1.amazonaws.com', port=56719, password='p989463208962d73f13be321bfdb81031166a01ea7a45d288c93276033c6fb4cf')
app = Flask(__name__)


@app.route('/', methods=['GET'])
def verify():
    # when the endpoint is registered as a webhook, it must echo back
    # the 'hub.challenge' value it receives in the query arguments
    if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == os.environ["VERIFY_TOKEN"]:
            return "Verification token mismatch", 403
        return request.args["hub.challenge"], 200

    return "Hello world", 200


@app.route('/', methods=['POST'])
def webhook():
    waki = Watson('c557270d-2fe2-4af8-acbb-761b6c50a9f8', 'ixRwdUE8i6te', '2017-05-26',
                  '125870c1-9dc7-483d-9aa1-1afd4952a795')
    # endpoint for processing incoming messaging events

    data = request.get_json()
    log(str(data) + '\n\n')  # you may not want to log every incoming message in production, but it's good for testing

    if data["object"] == "page":

        sender_idic = data['entry'][0]['messaging'][0]['sender']['id']
        log(r.get(sender_idic))
        log(sender_idic)
        if not r.get(sender_idic):
            r.set(sender_idic, 'hello')
        #if r.get(sender_idic) == 'done':
        #    send_message(sender_idic, 'Gotova prva faza, sada stize kredit')
        #    return 'ok', 200

        if (r.get(sender_idic) == 'hello') or (r.get(sender_idic) == 'pozdrav'):
            for entry in data["entry"]:
                for messaging_event in entry["messaging"]:

                    if messaging_event.get("message"):  # someone sent us a message

                        sender_id = messaging_event["sender"][
                            "id"]  # the facebook ID of the person sending you the message
                        recipient_id = messaging_event["recipient"][
                            "id"]  # the recipient's ID, which should be your page's facebook ID
                        try:
                            if 'text' in messaging_event["message"]:
                                message_text = messaging_event["message"]["text"]  # the message's text
                                watson_response = waki.message(message_text)
                                response_text = watson_response['output']['text'][0].encode('utf-8')
                                intent = watson_response['intents'][0]['intent']
                                log(intent)
                                if intent == 'izlaz':
                                    r.set(sender_idic, 'hello')
                                if intent == 'krediti_interesovanje':
                                    r.set(sender_idic, 'done')
                                elif intent == 'pozdrav':
                                    r.set(sender_idic, 'pozdrav')
                                elif intent == 'kupovina':
                                    r.set(sender_idic, 'kupac')
                                send_message(sender_id, response_text)
                                return 'ok', 200

                        except Exception, e:
                            log(e)
                            send_message(sender_id, 'Izvinite, mozete li mi ponoviti poruku!')
                            #r.set(sender_idic, 'hello')
                            return 'ok', 200
        if r.get(sender_idic) == 'kupac':
            for entry in data["entry"]:
                for messaging_event in entry["messaging"]:

                    if messaging_event.get("message"):  # someone sent us a message

                        sender_id = messaging_event["sender"]["id"]  # the facebook ID of the person sending you the message
                        recipient_id = messaging_event["recipient"]["id"]  # the recipient's ID, which should be your page's facebook ID
                        try:
                            if 'text' in messaging_event["message"]:
                                message_text = messaging_event["message"]["text"]  # the message's text
                                watson_response = waki.message(message_text)
                                response_text = watson_response['output']['text'][0].encode('utf-8')
                                intent = watson_response['intents'][0]['intent']
                                log(intent)
                                if intent == 'izlaz':
                                    r.set(sender_idic, 'hello')
                                if intent == 'krediti_interesovanje':
                                    r.set(sender_idic, 'done')
                                elif intent =='pozdrav':
                                    r.set(sender_idic, 'pozdrav')
                                elif intent == 'kupovina':
                                    r.set(sender_idic, 'kupac')
                                send_message(sender_id, response_text)
                                return 'ok', 200
                            elif 'attachments' in messaging_event["message"]:
                                foto_video = messaging_event["message"]['attachments'][0]['payload']['url']
                                image_response = GoogleWrapper.feature_finder(image=foto_video,
                                                                              detection_type='WEB', max_results=3)
                                score_results = Aladin(image_response).score_ok()
                                if score_results:
                                    r.set(sender_idic, 'image_ok')
                                    log(str(image_response))
                                    # ovde treba da nakacimo dalji pipe kada je slika ok.
                                    evaluator = KreditEvaluator(image_response[0]['Entitet'], sender_idic)
                                    price = evaluator.get_product_price()
                                    max_money = evaluator.get_max_money()
                                    log(str(price))
                                    if max_money >= price:
                                        #moze baki da kupi
                                        r.set(sender_idic, 'kredit_pregovori')
                                        send_message(sender_idic, 'Procena je da ovaj proizvod ima vrednost od '
                                                     + str(price) + ' sto vasa kreditna ocena dozvoljava da kupite')
                                        send_message(sender_idic, 'Mozete da podignete kredit u iznosu do ' +
                                                     str(max_money) + ' dinara bez cekanja, sada!')
                                        send_message(sender_idic, 'Koja suma vam je potrebna za ovu kupovinu?')
                                        """
                                        Ovde da dodamo sugestiju jos nekih proizvoda i pregovaranje oko kredita u narednom stanju
                                        """
                                else:
                                    log(str(image_response))
                                    r.set(sender_idic, 'kupac')
                                    send_message(sender_idic, 'Iz nekog razloga ne prepoznajemo predmet sa slike, '
                                                              'pa ako nije problem da dobijemo dodatnu sliku. '
                                                              'Pozdrav i hvala.')
                        except Exception, e:
                            log(e)
                            send_message(sender_id, 'Izvinite, mozete li mi ponoviti poruku!')
                            #r.set(sender_idic, 'hello')
                            return 'ok', 200

        if r.get(sender_idic) == 'kredit_pregovori':
            for entry in data["entry"]:
                for messaging_event in entry["messaging"]:

                    if messaging_event.get("message"):  # someone sent us a message

                        sender_id = messaging_event["sender"]["id"]  # the facebook ID of the person sending you the message
                        recipient_id = messaging_event["recipient"]["id"]  # the recipient's ID, which should be your page's facebook ID
                        try:
                            if 'text' in messaging_event["message"]:
                                message_text = messaging_event["message"]["text"]  # the message's text
                                watson_response = waki.message(message_text)
                                # response_text = watson_response['output']['text'][0].encode('utf-8')
                                intent = watson_response['intents'][0]['intent']
                                entities = watson_response['entities']
                                log(str(entities))
                                for entity in entities:
                                    if entity['entity'] == 'valuta':
                                        log(str(entity))
                                        valuta = entity['value']
                                        if valuta == 'evro':
                                            suma = suma * 122
                                    else:
                                        log(str(entity))
                                        suma = int(entity['value'])

                                evaluator = KreditEvaluator(user_id=sender_idic)
                                try:
                                    if suma > evaluator.get_max_money():
                                        send_message(sender_idic, 'Suma koju ste uneli se nalazi iznad '
                                                                  'dozvoljenog maksimuma :(')
                                        return 'ok', 200
                                except:
                                    send_message(sender_idic, 'Molimo unesite sumu koja vam je potrebna.')
                                    return 'ok', 200
                                send_message(sender_idic, 'Kes kredit u iznosu od ' + str(suma) +
                                             'RSD ce vam uskoro biti odobren.')
                                send_message(sender_idic, 'Da li je to uredu?')
                                r.set(sender_idic, 'cekanje_potvrde')
                                if intent == 'izlaz':
                                    r.set(sender_idic, 'hello')
                                return 'ok', 200

                        except Exception, e:
                            log(e)
                            send_message(sender_id, 'Izvinite, mozete li mi ponoviti poruku!')
                            #r.set(sender_idic, 'hello')
                            return 'ok', 200
        if r.get(sender_idic) == 'cekanje_potvrde':
            for entry in data["entry"]:
                for messaging_event in entry["messaging"]:

                    if messaging_event.get("message"):  # someone sent us a message

                        sender_id = messaging_event["sender"]["id"]  # the facebook ID of the person sending you the message
                        recipient_id = messaging_event["recipient"]["id"]  # the recipient's ID, which should be your page's facebook ID
                        try:
                            if 'text' in messaging_event["message"]:
                                message_text = messaging_event["message"]["text"]  # the message's text
                                watson_response = waki.message(message_text)
                                # response_text = watson_response['output']['text'][0].encode('utf-8')
                                intent = watson_response['intents'][0]['intent']
                                evaluator = KreditEvaluator(user_id=sender_idic)
                                if intent == 'potvrda':
                                    send_message(sender_idic, 'Hvala na odgovoru, '
                                                              'uskoro vam stize SMS sa sigurnosnim kodom.')
                                    SMSHandler.send_to_user(int(sender_idic))
                                    r.set(sender_idic, 'potvrdjen_usmeno')
                                    return 'ok', 200
                                if intent == 'negacija':
                                    send_message(sender_idic, 'Hvala na odgovoru.')

                                r.set(sender_idic, 'potvrdjen_usmeno')
                                if intent == 'izlaz':
                                    r.set(sender_idic, 'hello')
                                return 'ok', 200

                        except Exception, e:
                            log(e)
                            send_message(sender_id, 'Izvinite, mozete li mi ponoviti poruku!')
                            #r.set(sender_idic, 'hello')
                            return 'ok', 200

        if r.get(sender_idic) == 'potvrdjen_usmeno':
            for entry in data["entry"]:
                for messaging_event in entry["messaging"]:
                    log('OK STANJE')
                    if messaging_event.get("message"):  # someone sent us a message
                        log('1')
                        sender_id = messaging_event["sender"]["id"]  # the facebook ID of the person sending you the message
                        log('2')
                        try:

                            if 'text' in messaging_event["message"]:
                                log('3')
                                message_text = messaging_event["message"]["text"]  # the message's text
                                log('4')
                                watson_response = waki.message(message_text)
                                log('5')
                                # response_text = watson_response['output']['text'][0].encode('utf-8')
                                try:
                                    intent = watson_response['intents'][0]['intent']
                                except:
                                    intent = 'hello'
                                message_text = message_text.strip()
                                log('6')

                                if SMSHandler.check(int(sender_idic), str(message_text)):
                                    log('7')
                                    send_message(sender_idic, 'Kredit je potvrdjen, '
                                                              'uskoro vam stize novac na karticu')
                                    r.set(sender_idic, 'hello')
                                else:
                                    send_message(sender_idic, 'Uneti kod nije ispravan, probajte jos jednom.')
                                if intent == 'izlaz':
                                    r.set(sender_idic, 'hello')
                                return 'ok', 200

                        except Exception, e:
                            log(e)
                            send_message(sender_id, 'Izvinite, mozete li mi ponoviti poruku!')
                            #r.set(sender_idic, 'hello')
                            return 'ok', 200

    return "ok", 200


def send_message(recipient_id, message_text):
    log("sending message to {recipient}: {text}".format(recipient=recipient_id, text=message_text))

    params = {
        "access_token": os.environ["PAGE_ACCESS_TOKEN"]
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": recipient_id
        },
        "message": {
            "text": message_text
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        log(r.status_code)
        log(r.text)


def send_message_yes_or_no(recipient_id, message_text, image_url):
    log("sending message to {recipient}: {text}".format(recipient=recipient_id, text=message_text))

    params = {
        "access_token": os.environ["PAGE_ACCESS_TOKEN"]
    }
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "recipient": {
            "id": recipient_id
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "Ovde ide porucica",
                            "image_url": image_url,
                            "subtitle": "Ovde ide poporucica",
                            "buttons": [
                                {
                                    "type": "postback",
                                    "title": "Zelim",
                                    "payload": "DEVELOPER_DEFINED_PAYLOAD"
                                }, {
                                    "type": "postback",
                                    "title": "Ne zelim",
                                    "payload": "DEVELOPER_DEFINED_PAYLOAD"
                                }
                            ]
                        }
                    ]
                }
            }
        }
    })
    r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
    if r.status_code != 200:
        log(r.status_code)
        log(r.text)


def log(message):  # simple wrapper for logging to stdout on heroku
    print str(message)
    sys.stdout.flush()


if __name__ == '__main__':
    app.run(debug=True)
